%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%                            PREAMBLE                                 %
%                            ========                                 %
%                                                                     %
%    Setup for using AMS-LaTeX with natbib and a few other useful     %
%    packages. The preamble includes changes to the way sections,     %
%    subsections, the title, and similar headers are formatted.       %
%                                                                     %
%     * Paper size is handled through the geometry package            %
%     * Headers and footers via the fancyhdr package                  %
%     * Character encodings via the inputenc package
%
%     * Captions for tables and figures via the caption package       %
%     * Graphics inclusion and manipulation via the graphicx package  %
%     * Linespacing via the setspace package                          %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt,a4paper,reqno]{amsart}
\usepackage{fixltx2e}[2000/09/24]
\usepackage[T1]{fontenc}
\usepackage{geometry,amssymb}
\usepackage[longnamesfirst]{natbib}
\usepackage{listings}
\usepackage{setspace,enumerate,dcolumn}
\usepackage{graphicx}
\usepackage{rotating}
\usepackage[format=hang,labelsep=colon,labelfont=sc]{caption}
\usepackage[ansinew]{inputenc}
\usepackage{psfrag}
\usepackage{fancyhdr}
\usepackage{pdfpages}
\usepackage{color}
\usepackage{hyperref}
\usepackage{tipa}
\usepackage{adtrees}
\usepackage{float}
\restylefloat{table}















\usepackage{epstopdf}
\usepackage{pdflscape}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%                Set the linewidth and the margin size                %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\setstretch{1.4}
\geometry{margin=1in}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%           Defining the graphics path for eps-files                  %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\graphicspath{{./epsfiles/}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%                       Define the page styles                        %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%                Define and redefine a few math operators             %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\renewcommand{\vec}{\mbox{\rm vec}}
\newcommand{\trace}{\mbox{\rm tr}}
\newcommand{\diag}{\mbox{\rm diag}}
\newcommand{\vech}{\mbox{\rm vech}}
\newcommand{\rank}{\mbox{\rm rank}}
\newcommand{\argmin}{\mbox{\rm arg\,min}}
%

\newcommand{\rbs}[1]{\raisebox{-0.6ex}[+0.6ex]{$\scriptstyle #1$}}
\newcommand{\rbss}[1]{\raisebox{-0.5ex}[+0.5ex]{$\scriptscriptstyle #1$}}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,
    urlcolor=cyan,
}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%     Changing the default font for \title, \author, \section,        %
%     \subsection, and \subsubsection commands                        %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\makeatletter
%
% do not indent the first paragraph of sections, subsections, etc.
%
\let\@afterindenttrue\@afterindentfalse
\@afterindentfalse
%
% make sure that sub-sections and sub-sub-section headers are shown on
% separate lines and using a small cap font shape
%
%
% change the title to a large uppercase font
%

%
\title{Wage-Price-Output Gap- block \\
(WAPRO) }
\author{}\thanks{Prepared by N. Bokan, K. Christoffel, M. T\'{o}th, K. Ruppert, L. Rossi, A. Cheysson; Corresponding authors: Kai Christoffel, \href{mailto:kai.christoffel@ecb.int}{\nolinkurl{kai.christoffel@ecb.int}} and Nikola Bokan, \href{mailto:nikola.bokan@ecb.europa.eu}{\nolinkurl{nikola.bokan@ecb.europa.eu}} }

\date{\today}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                     %
%              End of preamble, beginning of the document             %
%                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\maketitle


\begin{abstract}
In this note we present the WAGE-PRICE-OUTPUT-GAP block of the ECB-MC. The modelling strategy is following a general equilibrium system approach, where the structure of the equations is based on microfoundations but the coefficients in the equations relax the structural restrictions. With WAPRO it is possible to identify the output gap and a NAIRU measure which will be used in the ECB-MC as well as the estimated structural equations for inflation and wage inflation. The model is estimated for the five large countries of the euro area and the estimations results are presented.
\end{abstract}



\section{Introduction}


\textbf{While the main part of the model follows the single equation estimation logic of the FRB-US approach, a system estimation approach is chosen for the joint determination of output gap, inflation, wages and unemployment.} This part formulates a semi-structural general equilibrium model, where the structure of the equations follows economic reasoning, but several economic restrictions are relaxed to improve the empirical fit and allow for an intuitive use of the model. This approach is very close to the approach chosen by the IMF in their 'QPM' series.\footnote{I. Carabenciov, I. Ermolaev,C. Freedman, M. Juillard,O. Kamenik, D. Korshunov,and Douglas Laxton,'A Small Quarterly Projection Model of the US Economy', IMF-WP/08/278, 2008}  The model is centered around a \textbf{Wa}ge-\textbf{Pr}ice and \textbf{O}utput gap (WAPRO  ) specification.

\textbf{The strategy of estimating the equations in the main model in subblocks, combined with a general equilibrium approach for the price, wage, output gap nexus allows to merge the advantages from two different modelling classes.} The modelling strategy of the main part of the ECB-MC follows the structure of the FRB-US and combines a high degree of flexibility, a good data-fit and an intuitive and easy use of the model. The equations of the model are estimated in small subsystems such as the investment block, the consumption block or the trade block. While this approach features the advantages discussed above, the system properties and general equilibrium properties of the model are introduced in a later step only, when the model parts are combined. To improve the system properties, especially in the nexus of prices, wages, output gap and interest rates, a small general equilibrium model is estimated. Key features of the approach such as Bayesian system estimation, some microfoundations and the explicit modelling of expectations are borrowed from the dynamic stochastic general equilibrium literature. However, several of the economic restrictions are relaxed to improve the data fit and to allow a for an easy and intuitive use of the model. While the structure of the equations is guided by optimizing behaviour of the economic agents,  the cross-equation restrictions of the structural parameters are ignored and instead reduced form estimates of the loading coefficients are estimated.

\textbf{The chosen framework allows for a Kalman filter based, general equilibrium estimation strategy in a Bayesian setting.}
The advantages of this approach relate to an explicit focus on system properties and the possibility to estimate unobserved concepts such as the output gap or measures of natural unemployment measures such as the NAIRU. The output gap in the main model will then be consistent with the estimated Phillips curves for price and wage inflation.

Furthermore the chosen approach is explicitly focussing on a multi-country setting in a monetary union. The clear and well defined framework of a general equilibrium model provides a structure to analyse various dynamic and stability issues that arise in the multi-country currency union setting and address these issues at an early stage of the development process.

In addition to this the WAPRO model features a clear structure with steady state and balance growth path. The model could serve as a workhorse to export these properties also to ECB-MC and the exercises conducted with the ECB-MC such as medium-term simulations.



\textbf{The model consistent estimates of unobserved variables such as the output gap and the equations for price and wage inflation are exported to the ECB-MC.} The WAPRO model relates to the ECB-MC is two respects. First, unobserved variables such as output gap and unemployment gap are estimated as state variables in the WAPRO model. This allows to identify these measures in a model consistent way and ensure a joint determination of output, prices, wages and various measures of slack. More concretely, the WAPRO implied measure of potential outputand the NAIRU will be exported to the ECB-MC. In the framework of the ECB-MC production function, conditional on the factor inputs, a series for technology growth is computed. In a forecasting or a simulation environment, the level of technology serves as an initial determination of potential output. However, potential output and the output gap remain endogenous to the dynamics of the ECB-MC.\footnote{The structure of WAPRO can be seen as an extension of the workhorse model of the ESCB Task force on potential output, with expectations and interest rates}  Second, the WAPRO model produces the equations and estimated parameters of prices and wages that will be exported to the ECB-MC. This approach ensures consistency between measures of slack and the price and wage setting behaviour of firms and households. Changes in the measures of slack have a direct impact on price and wage setting  decision, which in turn have an impact on aggregate demand and the output gap and employment.


\section{Formation of expectations}
\textbf{The model is operative under two different forms of expectation formation, rational expectation and VAR based expectations} Rational expectations, or model consistent expectations, are based on the solution of the model under the assumption that also the expected variables follow the internal logic of the model. In contrast to this VAR based expectations are constructed from  the forecast of a VAR. Rational expectations are sometimes criticized as being overly optimistic on the assumption that agents have a complete understanding of the economy and base their expectation on this understanding. Furthermore the ECB-MC will be run under various expectation formation processes and to account for this WAPRO is also estimated under VAR-based expectations. The underlying VAR is the identical to the base VAR estimated for the PAC specification of the demand and supply block in the ECB-MC. In this note we are focussing on the rational expectations version. In an update of this note the VAR based expectation version will be compared to the rational expectation version.






\section{The model}
The model is a two country model. The first country is defined as an individual country of the euro area, while the second country is constructed as the rest of the euro area (REA). The cross country relations between these two entities are kept simple and comprise, the joint determination of interest rates and an expenditure shifting argument leading to an influence of relative prices between the two countries on the demand for domestically produced goods in the individual country and in the REA. This expenditure shifting argument allows to ensure stability, in a system that cannot be stabilized by the euro area interest rate alone.\footnote{Compare Gali and Monacelli (2008) on stability in a currency union setting, see 'Optimal Monetary and Fiscal Policy in a Currency Union', Journal of International Economics, vol. 76, 2008, 116-132} To keep notation simple, all variables without a superscript are the individual euro area country while a star refers to the REA and joint variables of both countries are superscripted by 'EA'.\footnote{To reduce the complexity of the model, the modelling of the country specific REAs is simplified to contain only an IS and a price Phillips curve }
\subsection{Output}

The output gap is modelled via a reduced form IS curve:

\begin{eqnarray}
\hat{y}_{t}=\beta _{\hat{y},+1}^{\hat{y}}E_t \hat{y}_{t+1}+\beta _{\hat{y},-1}^{\hat{y}} \hat{y}_{t-1}
+\beta _{\Delta \hat{y},\star}^{\hat{y}} \Delta \hat{y}^{\star}  -\beta _{r\pi}^{\hat{y}} \left( \hat{\pi}_t-\hat{\pi}_t^{\star}  \right)     -\beta _{\hat{r}}^{\hat{y}%
}\hat{r}_{t}+e_{\hat{y},t}
\end{eqnarray}%

where $\hat{y}_{t}^{ }$ is the log deviation of output from its trend, $\hat{r}_{t}^{ }$ is the real interest rate gap, $\hat{\pi}_t$ and $\hat{\pi}_t^{\star}$ denotes domestic and foreign inflation,$\Delta \hat{y}^{\star}$ the change in foreign output gap  and $e_{%
\hat{y},t}^{ }$ is an error term. The structure of this equation follows the general structure of an IS curve under a form of real rigidities like habit formation in consumption. The gap between foreign and domestic inflation is introduced to approximate an expenditure switching argument. If domestic prices accelerate at a higher pace than foreign prices, the domestic economic is experiencing a reduction in price competitiveness and a lower demand for domestically produced goods. The change in foreign output proxies for the foreign demand for domestic products.
Trend growth ($\Delta \bar{y}_{t}^{ }$) is modelled via an AR process
\begin{eqnarray}
\Delta \bar{y}_{t}=\left( 1-\rho _{\Delta \bar{y}}\right) \Delta \bar{y}%
_{ss}+\rho _{\Delta \bar{y}}\Delta \bar{y}_{t-1}+e_{\Delta \bar{y},t}
\end{eqnarray}%

So that observed output growth can be matched to the model variables via the following measurement equation:
\begin{eqnarray}
\Delta y_{t}=\hat{y}_{t}-\hat{y}_{t-1}+\Delta \bar{y}_{t}
\end{eqnarray}%


\subsection{Inflation}

 Price inflation is modelled via a reduced form NK Phillips Curve, where inflation is modelled as a deviation from a time-varying inflation attractor ($\bar{\pi}$). The equation shows that actual inflation depends both on a measure of expected inflation and past inflation, the deviation of output from trend and wage inflation.\footnote{The inclusion of wage inflation as an additional variable in the inflation equation is producing stability in WAPRO. The introduction of this term will re-evaluated once the equation is exported to the ECB-MC. }
\begin{eqnarray}
\hat{\pi }_{t}=\beta _{\hat{\pi} }\hat{\pi} _{4,t+4}+(1-\beta _{\hat{\pi} })\hat{\pi} _{4,t-1}+\beta _{\hat{y}}^{\hat{\pi }}\hat{y}_{t-1}+\beta _{\hat{\pi}^{w}}^{\hat{\pi }}\hat{\pi}^{w}_{4,t}+e_{\hat{\pi} ,t}
\end{eqnarray}%

To improve the fit the lagged inflation variable is the annual inflation rate



\begin{eqnarray}
\hat{\pi} _{4,t}=\frac{1}{4}\left( \hat{\pi} _{t}+\hat{\pi} _{t-1}+\hat{\pi} _{t-2}+\hat{\pi}
_{t-3}\right)
\end{eqnarray}%




Note that inflation is modelled with reference to a country specific inflation attractor $\bar{\pi}$. The euro area inflation target is modelled in the Taylor rule for the euro area. The average inflation in the individual countries of the euro area deviates substantially from the euro area inflation target. To model this discrepancy the country specific inflation attractors are introduced as follows:
\begin{eqnarray}
\bar{\pi}_{t}=(1-\beta_{\bar{\pi}})\pi^{EA,\ast}_{ss,ss}+\beta_{\bar{\pi}}\bar{\pi}_{t-1}+\beta^{\bar{\pi}}_{\hat{\pi}_{4}}\hat{\pi}_{4,t}+e_{\bar{\pi} ,t}
 \end{eqnarray}%
Inherent to this specification is the property that repeated low inflation outcomes will drive down the country specific inflation attractor temporarily.
Finally, the level of inflation is computed using the inflation gap and the inflation attractor:
\begin{eqnarray}
\hat{\pi}_{t}=\pi_{t}-0.25 \bar{\pi}_{t}
 \end{eqnarray}%





\subsection{Wage Inflation}

The wage inflation gap is modelled via a reduced form NK Phillips wage curve. The equation shows that actual wage inflation gap depends on future wage inflation and past domestic price inflation, the deviation of trend output growth from its steady state and the change in the unemployment gap.




\begin{eqnarray}
\hat{\pi}_{t}^{w}=\begin{array}{c} \beta_{\hat{\pi}^{w}}\hat{\pi}_{t+1}^{w}++\xi_{\hat{\pi}^{w}}\hat{\pi}_{t-1}
+\beta_{\Delta \bar{y}}^{\hat{\pi}^{w}}\left(\Delta \bar{y}_{t}-\Delta \bar{y}_{ss} \right)-\beta_{u}^{\hat{\pi}^{w}}\left(\hat{u}_{t}-\hat{u}_{t-1} \right)-e_{\hat{\pi}_{t}^{w},t}
\end{array}
\end{eqnarray}%

\begin{eqnarray}
\pi_{4,t}^{w}=\hat{\pi}^{w} _{4,t}+{\pi}_{4,t}+\Delta \bar{y}_{t}
\end{eqnarray}%







\subsection{Unemployment}
The unemployment gap ($\hat{u}$) is defined as the difference between actual unemployment rate and the natural level of the unemployment rate ($\bar{u}$): $\hat{u}_{t}=u_{t}-\bar{u}_{t}$

 The unemployment gap follows a flexible interpretation of Ocun's law and is positively related to its level in $t-1$ and  it is negatively related to the output gap.
\begin{eqnarray}
\hat{u}_{t}=\rho _{\hat{u}}\hat{u}_{t-1}+\beta_{\hat{y}}^{\hat{u}}\hat{y}_{t}+e_{\hat{u},t}
 \end{eqnarray}%

The natural level of the unemployment rate follows an autoregressive structure in it's own level and the growth rate of natural unemployment ($GU_t$) which is modelled as an AR(1) process.\footnote{In the estimation of the model, this equation is extended for some countries by the lagged value of unemployment as a driver of natural unemployment. THis is done to bring the model implied NAIRU closer to the official NCB measures of the NAIRU.}
\begin{eqnarray}
\bar{u}_{t}=\left( 1-\rho _{\bar{u}}\right)  \bar{u}_{t-1}+\rho _{ \bar{u}} \bar{u}_{ss}+GU_{t} +e_{\bar{u},t}
 \end{eqnarray}%

\begin{eqnarray}
GU_{t}=\left(1-\rho _{GU}\right)GU_{t-1}+e_{GU,t}
 \end{eqnarray}%




\subsection{Interest Rates}
The real interest rate for the individual country is given by the difference between the nominal interest rate for the euro area and the expected domestic price inflation rate.
\begin{eqnarray}
r_{t}=i_{t}^{EA}-E_t\pi _{t+1}
\end{eqnarray}%
$\hat{r}_{t}^{ }$ is the log deviation of the real interest rate from its trend.
\begin{eqnarray}
\hat{r}_{t}=r_{t}-\bar{r}_{t}^{EA}
\end{eqnarray}%






\subsection{Policy Rule}

 The monetary authority sets the nominal interest rate according to  a policy rule which contains its value in the previous period,the nominal interest rate trend, a weighted average of home and foreign inflation and a weighted average of home and foreign output gaps.
\begin{eqnarray}
i_{t}^{EA}=\rho_{i,1}^{EA}i_{t-1}^{EA}+\left( 1-\rho_{1,1}^{EA}\right) \left\{
\begin{array}{c}
\bar{r}_{t}^{EA}+\pi _{t}^{\ast,EA} \\
+\rho_{i,2}^{EA}\left[ \omega_{t, \pi }4 \pi _{t}+\left( 1-\omega_{t, \pi }\right)4\pi
_{t}^{\star }-\pi _{t}^{\ast ,EA}\right]  \\
+\rho_{i,3}^{EA}\left[ \omega_{t, y}\hat{y}_{t}+\left( 1-\omega_{t, y}\right) \hat{y}_{t}^{\star }%
\right]
\end{array}%
\right\} +e_{i,t}^{EA}
\end{eqnarray}%

where the euro area inflation target is modelled as an AR process.
\begin{equation}
\pi_{t}^{\ast ,EA}=\rho _{\pi ^{\star }}^{EA}\pi _{t-1}^{\ast ,EA}+\left(
1-\rho _{\pi ^{\star }}^{EA}-\rho_{\bar{\pi} }^{EA}\right) \pi _{ss,ss}^{\ast ,EA}+
 \rho_{\bar{\pi} }^{EA}\left(\omega_{\pi }\pi_{4,t-1}+(1-\omega_{\pi })\pi _{4,t-1}^{ \star}  \right)+e_{\pi^{\ast},t}^{EA}
\end{equation}%

\begin{equation}
\pi_{4,t}^{EA}=\omega_{t, \pi}\pi_{4,t}+\left( 1-\omega_{t, \pi }\right) \pi_{4,t}^{,\star }
\end{equation}%

\begin{eqnarray}
\bar{r}_{t}^{EA}=\rho _{\bar{r}}^{EA}\bar{r} _{t-1}^{EA}+\left(
1-\rho _{\bar{r}}^{EA}\right) \bar{r} _{ss}^{EA}+e_{\bar{r},t}^{EA}
\end{eqnarray}%

The parameters $\omega_{t,i}$ give the wights of aggregating the output gap and inflation back to the euro area figures

\begin{eqnarray}
gap_{t}^{EA}=\omega_{t, 8y }\hat{y}_{t}+\left( 1-\omega_{t, y }\right) \hat{y}_{t}^{\star }
\end{eqnarray}%
\subsection{Rest of the euro area}

The rest of the euro area (REA) is defined as the euro area less the country modelled in the first block, and is therefore country-specific. To reduce complexity the REA is modelled only via the IS and the Phillips curve, the only two equations necessary to complete the system with the area wide determination of the interest rates.


\section{Data and estimation}

The model is estiamted on the following variables:
\begin{enumerate}
\item GDP deflator (individual country and REA)
\item GDP growth (individual country and REA)
\item unemployment (individual country)
\item growth rate of compensation per private employee (individual country)
\item euro area interest rate
\end{enumerate}

The first two variables are taken twice. Once for Germany (DE) and once for the rest of the euro area (REA). We  assume fixed weights for the individual country via the REA. The sample is 2000q1 to 2015q3.

A somewhat different approach was taken for Italy. Using the GDP deflator as the central pricing information resulted in a deterioration of model properties, in comparison to the other countries.\footnote{Especially the forecasting properties were disappointing.} To address this problem we follow Justiniano et. al.(2013) \footnote{ see A. Justitiano, G. E. Primiceri, and A. Tambalotti,'Is There a Trade-Off Between Inflation and Output Stabilization?', American Economic Journal: Macroeconomics, 5(2), April 2013, pp. 1-31.} and introduce an additional inflation series (HICP excluding energy) and define a small filtering system inside WAPRO.\footnote{More specifically, both observable inflation series are linked to the same model concept of inflation plus a measurement error for each equation: \begin{eqnarray*}
\pi_t^{4,yed}=\pi_t^{4,M}+\epsilon_t^{ME,yed}\\
\pi_t^{4,hex}=\pi_t^{4,M}+\epsilon_t^{ME,hex}
\end{eqnarray*}, where $M$ stands for the model based inflation, $YED$ for the GDP deflator and $HEX$ for HICP without energy. $ME$ stands for  measurement error. The idea of this approach is to define the two observed series as imperfectly, but driven by a joint component. The two measurement errors are estimated by the Kalman filter to identify the joint inflation driver}




%\includegraphics[height=12cm]{data1.jpg}
%\newline
%\includegraphics[height=12cm]{data2.jpg}

\section{Discussion of estimation results}

The remainder of this note is focussing on the results of the estimation and the analysis of the model properties evaluated at the posterior mode of the estimation.\footnote{In this version we are restricting the analysis to the version based on the posterior mode. Posterior distribution analysis is postponed to later version of this note.}


\subsection{Calibration and posterior mode estimates}
Note that not all parameters of the model have been estimated, but instead some of them are calibrated. This relates to the steady state values for the growth rate of output and the level of unemployment which are set according to the sample averages. Furthermore some parameters of the processes that determine the evolution of potential output and the NAIRU are calibrated to bring the implied output gap and NAIRU close to the levels of the official NCB numbers.

Tables \ref{params_DE} to \ref{params_NL} in the appendix give the parameter estimates at posterior mode. All parameter estimates are interior solutions albeit the priors are pretty tight for some parameters.\footnote{Please note that in the current version all analysis is based on the posterior mode estimation, while the posterior distribution will be discussed in the next version of this note.}

\subsection{Moments and correlations of the model evaluated at posterior mode}

Tables \ref{mean_DE} to \ref{mean_NL} in the appendix show the means and the standard deviation of the model simulation at posterior mode for the individual countries and compares them with the moments in the data. In general the means of the model are close to the data. This holds for most countries and most variables. However, there are some deviations from this for inflation and interest rates. Taking into consideration the inflation objective of the ECB (which is set at 1.8 \% annual inflation) and defining this as the mean of the inflation processes of all five country is implying a model mean considerably above or below the data mean for inflation and interest rates. In particular in comparison to the model mean, the data mean is too high in Germany (1.04 \% inflation), France (1.4\%) and the Netherlands (1.74 \%). In contrast to this the model mean is too low for Italy and Spain with data means of 2.1\% inflation in Spain and 2.0 \% inflation in Italy. To account for these differences in the mean of inflation, country specific, time varying inflation mean processes are introduced into the individual country models. Appendix subsections \ref{tcs_DE} to \ref{tcs_NL} display the evolution of these inflation process for the different countries .\footnote{Note that for the country specific rest of the euro area constructs we do not assume a time varying inflation mean. }

Turning to the standard deviations we observe a slight overestimation of the variance of model variables in comparison to their observed counterparts. This pattern holds for most variables and most countries. One exception is the volatility of inflation, wages and unemployment in Spain, where the model is actually producing less volatility than observed in the data. This is at least partially related to the fact that the variances of these series in Spain are considerably higher than the variances of the series in the other countries.

Tables \ref{correl_DE} to \ref{correl_NL} display the contemporaneous correlation between different model variables and compares those correlations with those in the data. In most of the cases the sign of the model correlation is the same as the sign of the correlation in the data. Focussing on the within country correlations the model fails to reproduce the positive correlation between inflation and ouptut growth observed in Spain and to some extend in the Netherlands. Furthermore the model is implying a positive correlation between output growth and inflation while this correlation is negative in Spain. For the Netherlands we get the opposite result of a negative correlation in the model and a positive correlation in the data.

\subsection{Impulse response functions}

\subsubsection{Monetary policy shock}
The impulse response functions in subsections \ref{irfs_DE} to \ref{irfs_NL} show the response of the individual countries to an unexpected shock to the euro area policy rule.
The impulse responses to an interest rate shock are in line with common wisdom regarding
the transmission of monetary policy. Domestic demand is temporarily curtailed and firms
cut back their demand for labour and, therefore, employment falls. Via its impact on firms'
marginal cost, the resulting decline in wages puts downward pressure on domestic prices.

While the impetus is the same for all countries and the qualitative response is very similar, there are notable differences in the quantitative response.\footnote{In the current version the euro area policy rule has been estimated for each country model and show some differences in the posterior mode estimates. This approach should be replaced by a a version with identical euro area interest rate setting.} More specifically we find relatively contained responses of inflation and output to a monetary policy shock for Germany and France. For Germany the response of the output gap is slightly more pronounced than in France while the opposite is true for the inflation response. The strongest inflation response is found Spain followed by Italy, where the peak response is reached after 7 quarters for Spain while it takes 15 quarters for the Italian inflation to reach its peak. On the output gap response both Spain and Italy display the strongest responses among the five countries.

\subsubsection{IS curve shock}

The second shock is an IS curve shock bearing the interpretation of a demand shock. In response to higher demand output increases and via the increased demand for labour wages and inflation increase. The central bank is reacting to higher output and inflation by increasing the interest rate.
The highest passthrough from demand to inflation is found for France, followed by the Netherlands,Spain, Germany and Italy, in descending order.


\subsection{In-sample forecasting}

We produce (pseudo) -recursive (or insample) forecasts in order to be able to evaluate the
forecasting performance of the model. In particular, we use the model with
estimated parameter values (estimated over the whole sample) and produce forecasts at specific historical points in time. We then compare these forecasts with the actual data realizations .
The procedure works as follows: In every forecasting period (ideally in the past), we
specify a subsample by keeping the starting point and fixing the end point to be the specific
period. We evaluate the likelihood over this subsample using a DSGE smoother. Starting
at the end of the subsample, we forecast for 12 periods. We repeat this procedure for every
period in the recursive forecasting sample (2005Q1 to 2016Q4) and report the resulting mean
forecast in the figures below. Along with the forecasts, we also report the time series or the
smoothed value (depending on the fact that some time series are not available) at every period.

In subsections \ref{rfcs_DE} to \ref{rfcs_NL}, the graphs for the recursive forecast (RFC) for annual inflation, annual wage inflation, unemployment and the output gap are displayed.

The RFC for German inflation is clearly showing the role of the time varying inflation mean as the attractor of the forecast in the medium-term. Over the sample form 2005 to 2016, there is a slight upward trend in German GDP deflator starting from 0.5. The RFC is tracking this upward trend and display a certain degree of persistence in the path towards its medium term attractor. Wage inflation is showing less persistence and, as for the case of inflation, a return to a medium term attractor of wage inflation. Both the output gap as well as unemployment display a gradual closing of the output respective unemployment gap.


Annual inflation is Spain is characterized by two different phases. In the early phase inflation is around 4\% until 2008/2009 where a disinflationary episode brings inflation to values around or slightly above zero. The medium term inflation attractor is capturing this inflation reduction only partially and in a delayed way. The implied forecast paths show a convergence to this attractor, subject to the inertia inherent in the estimated parameters. Wage developments in Spain are following the downward shift in inflation with some lags. Similarly to the case of Germany the wage inflation forecast for Spain is showing a relatively fast return to the medium term wage target. In comparison to Germany, the size of the output gap in Spain is much larger, implying a somewhat faster speed in closing the gap.

Forecast for French annual inflation is displaying some persistence and a return to levels around 1.9\% at the beginning of the forecast sample and to 1.5\% towards the end of the forecast sample. The forecasting performance for unemployment, wage inflation and the output gap is comparable to case of Spain.

As discussed above the modelling for Italian GDP deflator proofed to be difficult, because of a strong irregular component. Introducing a second measure of inflation in terms of the HICP excluding energy and food index, increased the forecast performance significantly. The forecast for Italian GDP inflation displays a high degree of persistence and a gradual return to the medium term inflation attractor. The forecast performance for wages, unemployment and the output gap is comparable to the case of Germany.

The forecast performance for the Netherlands is similar to the one of Germany for all four variables under consideration.

\subsection{Trend cycle decompositions}

Using a Kalman filter approach it is possible to conduct trend-cycle decompositions inside WAPRO. These decompositions are done on output/potential output, unemployment/NAIRU, inflation/medium term inflation trends and wages/medium term wage trends. While the latter two are mainly introduced to improve the dynamics of the model and to reduce forecast errors, potential output and NAIRU will be used inside the ECB-MC. Subsections \ref{tcs_DE} to \ref{tcs_NL} show the trend-cycle decompositions for output, unemployment inflation and wage-inflation for the four countries.

The output gap measures identified by the mode are comparable to the official NCB numbers as can be seen from the picture on the right hand side in the first row of respective country sections in appendix \ref{tcs_DE} to \ref{tcs_NL}.

The discrepancies for the model implied NAIRU measures and their official NCB counterparts are larger than for the output gap. This is also related to specific labour market development which drive a wedge between observed unemployment and the NAIRU. The Kalman filtered approach to identify the NAIRU is missing some of these points leading to some discrepancies.




\input{appendix}

\end{document}

\begin{minipage}{1\linewidth}
\singlespacing
\emph{Notes:} The first two graphs show the model based NAIRU and output gap (red line) and the official NCB counterpart (blue line). The remaining graphs show the trend cycle decomposition for inflation, unemployment and output.
\end{minipage}
