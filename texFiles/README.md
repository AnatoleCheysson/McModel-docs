convert tex file into a rst file using pandoc


`pandoc -s -t rst --toc FB_docu_ver4.tex -o FB_docu_ver4.rst`

remove the LATEX related rst things on the top of the file

.. role:: math(raw)
   :format: html latex
..

.. contents::
   :depth: 3
..

Remove indentation of the line after all equations.


Equations should be labeled like this:

.. math::
   :label: MND

        \ln(XTD) = \alpha_{0} + \alpha_{xtd} \ln(XXD) + (1-\alpha_{xtd}) \ln(XND)

\hat is distorted in the output, use \widehat instead
