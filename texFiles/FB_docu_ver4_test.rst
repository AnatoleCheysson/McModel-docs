.. role:: math(raw)
   :format: html latex
..

.. contents::
   :depth: 3
..

Foreign Block
=============

The foreign sector (see Figure [fig:flowchart]) comprises overall 8
behavioural equations (total and extra, volumes and prices). Where
volume equations of exports (imports) depend on external (internal)
demand and competitiveness indicators based on weighted averages of
indicators for the main trading partners. Trade prices depend on costs,
competitor prices, exchange rate and oil for import prices. It is
modelled as Error Correction (EC) equations using Engle-Granger’s
two-step estimation approach. First step estimation of long run
relationship uses fully modified least squares supplemented by tests for
the stationarity of the residuals and allows for cointegration tests
imposing homogeneity. Step two estimates the short run dynamics. In
addition, this sector contains accounting identities to derive intra
trade, the current account balance and net foreign asset positions.

| |Structure of the Foreign Block|

Exports and imports of goods and services
-----------------------------------------

Exports of goods and services, volumes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We assume homogeneity between total exports and total foreign demand in
the long run ([eq:XTR\_long]), that is, demand elasticities are
constrained to 1 (expect for Spain where there has been a gradual
increase in its export market share). Export market share depend on
price competitiveness, while the long run structural changes in export
composition coming from other than price competitiveness are modelled
with use of linear trend (with break for some countries).

Meanwhile, the short run dynamics of intra and extra euro area exports
must differ due to different impact of exchange rate changes on relative
prices on one hand, and different contracts on the other. Therefore, we
include foreign demand intra and extra euro area separately into the
short run equation ([eq:XTR\_short]), and allow different impact of
exchange rate and competitors’ prices on export volumes.

.. math::

   \begin{aligned}
     \label{eq:XTR_long}
     \ln(XTR^{LR}) &= \gamma_{0} + \ln(WDR) + \gamma_{1} \ln\left(XTD/CXD\right) + \gamma_{2} Trend      \\
     \label{eq:XTR_short}
     \Delta\ln(XTR) &= \beta_{0} + \beta_{1} \Delta\ln(WDR^{IN}) + \beta_{2} \Delta\ln(WDR^{EX})     \nonumber \\
         &+ \beta_{3} \Delta\ln(CXD^{IN}) + \beta_{4} \Delta\ln(CXD^{EX}/EENX)                       \nonumber \\
         &+ \beta_{5} \Delta\ln(EENX) + \beta_{6} \Delta\ln(EENX_{-1}) + \beta_{7} \Delta\ln(XTD)    \nonumber \\
         &+ \beta_{8}\left(\ln(XTR_{-1}) - \ln(XTR^{LR}_{-1})\right) + \epsilon^{X}\end{aligned}

 where :math:`XTR` is total real exports, :math:`WDR` is real world
demand for export goods and services of the country, which is divided
into world demand from intra and extra euro area (:math:`WDR^{IN}` and
:math:`WDR^{EX}`, respectively). The relative prices are determined as
the share of export deflator (:math:`XTD`) to the competitors’ prices in
euro (:math:`CXD`).

The best specifications for export volumes were chosen taking into
account statistical significance, in-sample fit, fit to the BMEs,
historical growth decomposition, and block properties (for Germany).
These are the following.

+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
|                      | Germany         | France           | Italy           | Spain            | Netherlands     |
+======================+=================+==================+=================+==================+=================+
| C                    | 15.39\*\*\*     | 17.96\*\*\*      | 13.96\*\*\*     | 14.5\*\*\*       | 14.98\*\*\*     |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| Lin.trend            |                 | -0.0057\*\*\*    | -0.0074\*\*\*   | -0.005\*\*\*     | -0.0006\*\*\*   |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| World dem.           | 1               | 1                | 1               | 1.44\*\*\*       | 1               |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| Rel.PX               | -0.62\*\*\*     | -1.26\*\*\*      | -0.42\*\*\*     | -0.65\*\*        | -0.72\*\*\*     |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| D                    |                 | -0.381\*\*\*     | -0.414\*\*\*    | 0.293\*\*\*      |                 |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| D\*Lin.trend         |                 | 0.0053\*\*\*     | 0.006\*\*\*     | -0.0074 \*\*\*   |                 |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| C                    | 0               | 0                | -0.008\*\*\*    | 0.001            | 0               |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| ECM                  | -0.21\*\*\*     | -0.11\*\*        | -0.21\*\*       | -0.16\*\*\*      | -0.43\*\*\*     |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| D(WDR)               | 1.02\*\*\*      |                  |                 |                  |                 |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| d(WDRIN)             |                 | 0.5\*\*\*        | 0.56\*\*\*      | 0.47\*\*\*       | 0.57\*\*\*      |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| d(WDREX)             |                 | 0.36\*\*\*       | 0.6\*\*\*       | 0.6\*\*\*        | 0.41\*\*\*      |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| d(XTD)               |                 |                  | -0.51\*\*\*     | -0.51\*\*\*      | -0.48\*\*\*     |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| d(CXD)               | 0.17\*\*\*      |                  | 0.35\*\*\*      |                  | 0.31\*\*        |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| d(EENX)              |                 | 0.14             |                 |                  |                 |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| d(EENX(-1))          |                 | 0.27\*\*         |                 |                  |                 |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| Adj. R-squared       | 0.735           | 0.712            | 0.747           | 0.522            | 0.682           |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| S.E. of regression   | 0.0125          | 0.0099           | 0.0127          | 0.0159           | 0.0111          |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| Sum squared resid    | 0.013           | 0.008            | 0.0131          | 0.0206           | 0.0101          |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| Durbin-Watson stat   | 2.15            |                  |                 |                  |                 |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| Sample (adjusted)    | 1995Q2 2016Q4   | 1995Q3 2016Q4    | 1995Q2 2016Q4   | 1995Q2 2016Q4    | 1995Q2 2016Q4   |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
| Method               | Long run:       | Fully Modified   | Least Squares   | (FMOLS)          |                 |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+
|                      | Short run:      | Least Squares    |                 |                  |                 |
+----------------------+-----------------+------------------+-----------------+------------------+-----------------+

Table: Estimates for export equation specification, 2-step estimation

where break in developments of relative ULC (:math:`D^{10}`) and crisis
dummy (:math:`D^{09}`) are used for France and Italy, respectively. For
Spain, we use we try to catch domestic cycles with use of breaks
:math:`D^{0408}`.

Exports of goods and services, extra euro area, volumes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Extra euro area export volumes are modelled similarly to total exports.

.. math::

   \begin{aligned}
     \label{eq:XXR_long}
     \ln(XXR^{LR}) &= \gamma_{0} + \ln(WDR^{EX}) + \gamma_{1} \ln\left(XXD/CXD^{EX}\right) + \gamma_{2}  Trend   \\
     \label{eq:XXR_short}
     \Delta\ln(XXR) &= \beta_{0} + \beta_{1} \Delta\ln(WDR^{EX}) + \beta_{2} \Delta\ln(XXD)                 \nonumber \\
         &+ \beta_{3} \Delta\ln(CXD^{EX}/EENX) + \beta_{4} \Delta\ln(EENX)                                  \nonumber \\
         &+ \beta_{5}\left(\ln(XXR_{-1}) - \ln(XXR^{LR}_{-1})\right) + \epsilon^{XX}\end{aligned}

 where :math:`XXR` is total real exports, :math:`WDR^{EX}` is extra euro
area world demand for export goods and services of the country. Price
competitiveness is determined as the share of extra euro area export
deflator (:math:`XXD`) to the competitors’ prices in euro
(:math:`CXD^{EX}`). In the short run, we use nominal effective exchange
rate (:math:`EENX`).

+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                      | Germany         | France           | Italy           | Spain           | Netherlands     |
+======================+=================+==================+=================+=================+=================+
| C                    | 14.13\*\*\*     | 12.78\*\*\*      | 13.66\*\*\*     | 13.21\*\*\*     | 14.92\*\*\*     |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Lin.trend            | 0.0009\*\*\*    | -0.0092\*\*\*    | -0.0087\*\*\*   | -0.0012\*       | 0.0018\*\*\*    |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| World dem.           | 1               | 1                | 1               | 1               | 1               |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Rel.PX               | -0.46\*\*\*     | -0.22\*          | -0.46\*\*\*     | -0.59\*\*       | -0.91\*\*\*     |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| D                    |                 | -0.665\*\*\*     | -0.511\*\*\*    |                 |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| D\*Lin.trend         |                 | 0.0104\*\*\*     | 0.0075\*\*\*    |                 |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| C                    | 0.003           | 0                | -0.008\*\*      | 0               | 0.004           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| ECM                  | -0.32\*\*\*     | -0.32\*\*\*      | -0.47\*\*\*     | -0.09\*         | -0.15\*\*\*     |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(WDREX)             | 0.878\*\*\*     | 0.69\*\*\*       | 1.14\*\*\*      | 0.94\*\*\*      | 0.89\*\*\*      |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(WDREX(-1))         |                 |                  |                 |                 |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(XXD)               |                 |                  |                 |                 | -0.53\*\*\*     |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(CXDEX)             | 0.23\*\*\*      |                  |                 |                 | 0.43\*\*\*      |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(EENX)              |                 | 0.52\*\*\*       | 0.53\*\*\*      |                 |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Adj. R-squared       | 0.541           | 0.658            | 0.622           | 0.303           | 0.44            |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| S.E. of regression   | 0.0208          | 0.0131           | 0.0194          | 0.027           | 0.0271          |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sum squared resid    | 0.036           | 0.0108           | 0.0311          | 0.0614          | 0.0574          |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Durbin-Watson stat   | 2.18            | 2.07             | 2.24            | 2.1             | 2.14            |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sample (adjusted)    | 1995Q2 2016Q4   | 2000Q2 2016Q4    | 1995Q2 2016Q4   | 1995Q2 2016Q4   | 1996Q2 2016Q4   |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Method               | Long run:       | Fully Modified   | Least Squares   | (FMOLS)         |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                      | Short run:      | Least Squares    |                 |                 |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+

Table: Estimates for extra euro area export equation specification,
2-step estimation

Exports of goods and services, intra euro area, volumes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculated via identity.

.. math::

   \label{eq:XNR}
     \ln(XTR) = \alpha_{0} + \alpha_{xtr} \ln(XXR) + (1-\alpha_{xtr}) \ln(XNR)

 where the share of extra euro area volumes is the following. (The
values are calculated based on what is used in the projections.)

-2.5cm-2.5cm

+------------------------+-----------+----------+---------+---------+---------------+
|                        | Germany   | France   | Italy   | Spain   | Netherlands   |
+========================+===========+==========+=========+=========+===============+
| :math:`\alpha_{xtr}`   | 0.65      | 0.57     | 0.58    | 0.47    | 0.48          |
+------------------------+-----------+----------+---------+---------+---------------+

Imports of goods and services, volumes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We assume homogeneity between real imports and import demand in the long
run ([eq:MTR\_long]), that is, demand elasticity of imports of :math:`1`
to domestic absorption indicator that is constructed as a weighted
average of the final demand components. The weights (:math:`\kappa`)
reflect the import content of each demand component and are calculated
using the Input-Output tables. Import decisions depend on relative
prices, to which import price deflator excluding energy is taken into
account, as domestic demand for fuel could be considered inelastic over
our forecasting horizon. Taking a closer look at the trade data, a
steady and simultaneous increase over our sample period is visible in
the share of exports and imports in GDP due to increase in openness. To
capture this effect, we included an indicator of trade openness, namely
the share of exports to the domestic absorption indicator
(:math:`XTR/WER`), in the long run. Furthermore, for Italy and Spain,
there was a large overheating in the domestic economy before the crisis,
which might have added a cyclical development in import content to its
average value across time. This is proxied by the share of investment in
domestic absorption as the volatility of the procyclical investment is
larger than other domestic demand components.

In the short run, marginal import content of demand items may differ
from their average import content (i.e. the weights used to calculate
the domestic absorption indicator). Therefore, we included each demand
component separately in the dynamic equation ([eq:MTR\_short]).

.. math::

   \begin{aligned}
     \label{eq:MTR_long}
     \ln(MTR^{LR}) &= \gamma_{0} + \ln(WER) + \gamma_{1} \ln\left(MTD^{NO}/YED\right) +    \nonumber \\
         &+ \gamma_{2} \ln\left(XTR/WER\right) + \gamma_{3} \ln\left(ITR/WER\right)                  \\
     \label{eq:MTR_short}
     \Delta\ln(MTR) &= \beta_{0} + \beta_{1} \Delta\ln(MTR_{-1}) + \beta_{2} \Delta\ln(\kappa^{10}_C PCR)   \nonumber \\
         &+ \beta_{3} \Delta\ln(\kappa^{10}_G GCR) + \beta_{4} \Delta\ln(\kappa^{10}_I ITR)
            \beta_{5} \Delta\ln(\kappa^{10}_X XTR)                                                          \nonumber \\
         &+ \beta_{6}\left(\ln(MTR_{-1}) - \ln(MTR^{LR}_{-1})\right) + \epsilon^{M}\end{aligned}

 where :math:`MTR` is total real imports, :math:`WER` is domestic
absorption indicator, :math:`YED` is GDP deflator at market prices, and
:math:`MTD^{NO}` is the imported price deflator excluding energy.
:math:`XTR`, :math:`PCR`, :math:`GCR` and :math:`ITR` are the
expenditure items of GDP (real exports, private consumption, government
consumption and total investment, respectively). :math:`\kappa`\ ’s are
their import content average across time, and :math:`\kappa^{10}`\ ’s
are their most current value from 2010.

+-------------------------+-----------+----------+---------+---------+---------------+
|                         | Germany   | France   | Italy   | Spain   | Netherlands   |
+=========================+===========+==========+=========+=========+===============+
| :math:`\kappa_c`        | 0.188     | 0.190    | 0.183   | 0.209   | 0.248         |
+-------------------------+-----------+----------+---------+---------+---------------+
| :math:`\kappa_g`        | 0.080     | 0.081    | 0.069   | 0.098   | 0.108         |
+-------------------------+-----------+----------+---------+---------+---------------+
| :math:`\kappa_i`        | 0.292     | 0.242    | 0.265   | 0.286   | 0.355         |
+-------------------------+-----------+----------+---------+---------+---------------+
| :math:`\kappa_x`        | 0.389     | 0.301    | 0.286   | 0.352   | 0.554         |
+-------------------------+-----------+----------+---------+---------+---------------+
| :math:`\kappa^{10}_c`   | 0.200     | 0.198    | 0.201   | 0.252   | 0.282         |
+-------------------------+-----------+----------+---------+---------+---------------+
| :math:`\kappa^{10}_g`   | 0.087     | 0.095    | 0.081   | 0.115   | 0.117         |
+-------------------------+-----------+----------+---------+---------+---------------+
| :math:`\kappa^{10}_i`   | 0.360     | 0.254    | 0.262   | 0.313   | 0.360         |
+-------------------------+-----------+----------+---------+---------+---------------+
| :math:`\kappa^{10}_x`   | 0.439     | 0.321    | 0.331   | 0.475   | 0.613         |
+-------------------------+-----------+----------+---------+---------+---------------+

Table: Share of import content of expenditure items

The best specifications for import volumes were chosen taking into
account statistical significance, in-sample fit, and historical growth
decomposition. These are the following.

+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                      | Germany         | France           | Italy           | Spain           | Netherlands     |
+======================+=================+==================+=================+=================+=================+
| C                    | -1.02\*         | -3.12\*\*\*      | -4.28\*\*\*     | -2.52\*\*\*     | -1.16           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| WER                  | -1              | -1               | -1              | -1              | -1              |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Rel.PX               | -0.2\*          | -0.62\*\*\*      | -0.94\*\*\*     | -0.53\*\*\*     | -0.21           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| XTR/WER              | 0.59\*\*\*      | 0.77\*\*\*       | 0.51\*\*\*      | 0.002           | 0.66            |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| C                    | 0.003\*         | 0.003\*\*        | 0.002           | 0.001           | 0.001           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| ECM                  | -0.27\*\*\*     | -0.34\*\*\*      | -0.1\*\*\*      | -0.06           | -0.04           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| AR(1)                |                 | 0.22\*\*         | 0.21\*\*\*      |                 |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(PCR)               | -0.2            | -0.2             | -0.2            | -0.25           | -0.28           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(GCR)               | -0.09           | -0.1             | -0.08           | -0.12           | -0.12           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(ITR)               | -0.36           | -0.25            | -0.26           | -0.31           | -0.36           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(XTR)               | -0.44           | -0.32            | -0.33           | -0.48           | -0.61           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Adj. R-squared       | 0.527           | 0.652            | 0.605           | 0.667           | 0.79            |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| S.E. of regression   | 0.0147          | 0.0106           | 0.0149          | 0.017           | 0.0087          |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sum squared resid    | 0.0185          | 0.0094           | 0.0184          | 0.0245          | 0.0064          |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Durbin-Watson stat   | 2.29            | 2.31             | 2.23            | 1.85            | 1.77            |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sample (adjusted)    | 1995Q2 2016Q4   | 1995Q3 2016Q4    | 1995Q3 2016Q4   | 1995Q2 2016Q4   | 1995Q2 2016Q4   |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Method               | Long run:       | Fully Modified   | Least Squares   | (FMOLS)         |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                      | Short run:      | Least Squares    |                 |                 |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+

Table: Estimates for import equation specification, 2-step estimation

Imports of goods and services, extra euro area, volumes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Extra euro area imports are modelled as shares of total imports. The
ratio is supposed to be changed due to change in relative non-oil import
price (extra to total), and different impact of openness on extra and
intra imports due to different extent of change in deepening of the
global value chain on intra and extra euro area markets.

Due to lack of data on non-oil extra import prices, relative non-oil
import price (extra to total) is proxied by relative prices on
competitors’ markets.

.. math::

   \begin{aligned}
     \label{eq:MXR_long}
     \ln(MXR^{LR}) &= \gamma_{0} + \log(MTR) + \gamma_{1} \log\left(CMD^{EX}/CMD\right) +        \nonumber \\
          &+  \gamma_{2} \log\left(MTR/WER\right)                                                          \\
     \label{eq:MXR_short}
     \Delta\log(MXR) &= \beta_{0} + \beta_{1} \Delta\log(MTR)                                    \nonumber \\
          &+ \beta_{2} \left(\ln(MXR_{-1}) - \ln(MXR^{LR}_{-1})\right) + \epsilon^{MX}\end{aligned}

+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                      | Germany         | France           | Italy           | Spain           | Netherlands     |
+======================+=================+==================+=================+=================+=================+
| C                    | 4.34\*\*\*      | 5.49\*\*\*       | 3.85\*\*\*      | 1.71\*\*\*      | 2.14            |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| MTR                  | -1              | -1               | -1              | -1              | -1              |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Rel.PM               | -1.04\*\*\*     | -1.18\*\*\*      | -0.98\*\*\*     | -0.49\*\*\*     | -0.57\*\*       |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| MTR/WER              | -0.32\*\*\*     |                  |                 |                 | -1.04\*\*\*     |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| C                    | 0\*             | 0                | 0               | 0               | 0               |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| ECM                  | -0.36\*\*\*     | -0.14\*          | -0.25\*\*\*     | -0.17\*\*\*     | -0.15\*\*       |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(MND/MTDNO)         |                 | -0.65\*\*\*      | -0.2            | -0.22\*         | -0.35\*\*\*     |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(PCR)               |                 |                  |                 |                 | -0.31           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(GCR)               |                 |                  | 0.64\*\*\*      |                 |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(ITR)               |                 |                  |                 |                 | 0.14\*\*        |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(XTR)               |                 |                  | 0.13\*          |                 | -0.35\*\*\*     |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Adj. R-squared       | 0.73            | 0.524            | 0.701           | 0.8             | 0.483           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| S.E. of regression   | 0.0125          | 0.0185           | 0.0151          | 0.0169          | 0.0133          |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sum squared resid    | 0.0133          | 0.0218           | 0.0187          | 0.024           | 0.0136          |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Durbin-Watson stat   | 2.19            | 1.8              | 2.19            | 2.29            | 2.08            |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sample (adjusted)    | 1995Q2 2016Q4   | 2000Q2 2016Q4    | 1995Q2 2016Q4   | 1995Q2 2016Q4   | 1996Q2 2016Q4   |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Method               | Long run:       | Fully Modified   | Least Squares   | (FMOLS)         |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                      | Short run:      | Least Squares    |                 |                 |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+

Table: Estimates for extra euro area import equation specification,
2-step estimation

Imports of goods and services, intra euro area, volumes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculated via identity.

.. math::

   \label{eq:MNR}
     \ln(MTR) = \alpha_{0} + \alpha_{mtr} \ln(MXR) + (1-\alpha_{mtr}) \ln(MNR)

 where the share of extra euro area volumes is the following. (The
values are calculated based on what is used in the projections.)

+------------------------+-----------+----------+---------+---------+---------------+
|                        | Germany   | France   | Italy   | Spain   | Netherlands   |
+========================+===========+==========+=========+=========+===============+
| :math:`\alpha_{mtr}`   | 0.62      | 0.51     | 0.53    | 0.51    | 0.62          |
+------------------------+-----------+----------+---------+---------+---------------+

Exports of goods and services, prices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We assume that extended version of Law of One Price (LOOP) holds for
export prices in the long run ([eq:XTD\_long]). That is, the price
mark-up over competitors’ prices can be shifted due to change in cost
competitiveness, and structural changes (technological composition,
economies of scale, GVC). Pricing of exports depends on to how much
extent is the export sector price taker on average. If it is more price
taker, then cost competitiveness have no significant impact on export
prices (lower value of :math:`\alpha_{1}`). But if it is more price
setter, then cost competitiveness plays bigger role in pricing (larger
:math:`\alpha_{1}`).

In the short run, while prices accommodate to their trends, we let them
to react differently to changes in costs of domestic
(:math:`\Delta YED`) and imported goods (:math:`\Delta MTD`), exchange
rate (:math:`\Delta EENX`), as well as external
(:math:`\Delta (CXD^{EX}/EENX)`) and internal competitors’ prices
(:math:`\Delta CXD^{IN}`).

.. math::

   \begin{aligned}
     \label{eq:XTD_long}
     \ln(XTD^{LR}) &= \alpha_{0} + \ln(CXD)
              + \alpha_{1} \left(\left[\kappa_X \ln(MTD) + (1-\kappa_X)\ln(YED)\right] - ln(CXD)\right)     \nonumber \\
          &+ \alpha_{2} \ln\left(XTR/WDR\right)                                                                       \\
     \label{eq:XTD_short}
     \Delta\ln(XTD) &= \beta_{0} + \beta_{1} \Delta\ln(CXD^{IN}) + \beta_{2} \Delta\ln(CXD^{EX}) + \beta_{3} \Delta\ln(MTD)  \nonumber \\
          &+ \beta_{4} \Delta\ln(YED) + \beta_{5} \left(\ln(XTD_{-1}) - \ln(XTD^{LR}_{-1})\right) + \epsilon^{PX}\end{aligned}

The best specifications for export deflator were chosen taking into
account statistical significance, in-sample fit, and historical growth
decomposition. These are the following.

+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                        | Germany         | France           | Italy           | Spain           | Netherlands     |
+========================+=================+==================+=================+=================+=================+
| C                      | 4.65            | 3.27\*\*\*       | -0.17           | 1.62\*\*\*      | 2\*\*\*         |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Competitor prices      | -1              | (1-0.30)         | (1-0.83)        | (1-0.66)        | (1-0.57)        |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Cost competitiveness   |                 | 0.3              | 0.83\*\*\*      | 0.66\*\*\*      | 0.57\*\*\*      |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Market power           |                 |                  | 0.09\*\*\*      |                 |                 |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| C                      | 0               | -0.001           | 0.001           | 0.001\*         | -0.001          |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| ECM                    | -0.01\*\*       | -0.02\*\*        | -0.26\*\*\*     | -0.72\*\*\*     | -0.07\*         |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(CXD)                 |                 | 0.07\*\*\*       |                 | 0.23\*\*\*      | 0.22\*\*\*      |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(CXDEX-EENX)          | 0.03\*\*        |                  |                 |                 |                 |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(EENX)                | 0.03\*\*        |                  |                 |                 |                 |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(MTD)                 | 0.33\*\*\*      | 0.43\*\*\*       | 0.35\*\*\*      | 0.22\*\*\*      | 0.67\*\*\*      |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(YFD)                 | 0.15\*          | 0.29\*\*         | 0.29\*          | 0.24\*\*\*      | 0.26\*\*        |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Adj. R-squared         | 0.797           | 0.753            | 0.593           | 0.581           | 0.833           |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| S.E. of regression     | 0.0023          | 0.0035           | 0.0053          | 0.007           | 0.0061          |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sum squared resid      | 0.0005          | 0.001            | 0.0023          | 0.0041          | 0.0031          |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Durbin-Watson stat     | 2.24            | 2.24             | 2.14            | 1.88            | 2.81            |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sample (adjusted)      | 1995Q2 2016Q4   | 1995Q2 2016Q4    | 1995Q2 2016Q4   | 1995Q2 2016Q4   | 1995Q2 2016Q4   |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Method                 | Long run:       | Fully Modified   | Least Squares   | (FMOLS)         |                 |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                        | Short run:      | Least Squares    |                 |                 |                 |
+------------------------+-----------------+------------------+-----------------+-----------------+-----------------+

Table: Estimates for export price equation specification, 2-step
estimation

Exports of goods and services, extra euro area, prices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Extra euro area export prices are modelled similarly to total export
prices.

.. math::

   \begin{aligned}
     \label{eq:XXD_long}
     \ln(XXD^{LR}) &= \alpha_{0} + \ln(CXD^{EX})
              + \alpha_{1} \left(\left[\kappa_X \ln(MTD) + (1-\kappa_X)\ln(YED)\right] - ln(CXD^{EX})\right)  \nonumber \\
          &+ \alpha_{2} \ln\left(XXR/WDR^{EX}\right)                                                                    \\
     \label{eq:XXD_short}
     \Delta\ln(XXD) &= \beta_{0} + \beta_{1} \Delta\ln(CXD^{EX}) + \beta_{2} \Delta\ln(MTD) + \beta_{3} \Delta\ln(YED)  \nonumber \\
          &+ \beta_{4} \left(\ln(XXD_{-1}) - \ln(XXD^{LR}_{-1})\right) + \epsilon^{PXX}\end{aligned}

+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                       | Germany         | France           | Italy           | Spain           | Netherlands     |
+=======================+=================+==================+=================+=================+=================+
| C                     | 4.66            | -1\*\*\*         | -0.62           | 1.97\*\*\*      | 2.9             |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Competitors’ prices   | 1               | 0.23\*\*\*       | 0.09\*\*\*      | 0.42\*\*\*      | 0.22            |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Relative costs        |                 | (1-0.23)         | (1-0.09)        | (1-0.42)        | (1-0.22)        |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Market structure      |                 | 0.19\*\*\*       | 0.1\*\*\*       |                 | -0.17           |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| C                     | -0.001          | 0                | 0.003\*\*       | 0.003           | -0.001          |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| ECM                   | -0.01           | -0.19\*\*        | -0.2\*          | -0.38\*\*       | -0.64           |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(CXDEX)              | 0.11\*\*\*      | 0.09\*\*\*       |                 | 0.15\*\*        |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(MTD)                | 0.16\*\*\*      | 0.38\*\*\*       | 0.33\*\*\*      | 0.32\*\*\*      | 1               |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(YFD)                | 0.36\*\*        | 0.36             |                 |                 | 0.4             |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Adj. R-squared        | 0.563           | 0.554            | 0.347           | 0.369           | 0.748           |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| S.E. of regression    | 0.0038          | 0.0053           | 0.0083          | 0.0143          | 0.0122          |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sum squared resid     | 0.0012          | 0.0018           | 0.0058          | 0.017           | 0.0117          |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Durbin-Watson stat    | 2.49            | 2.19             | 2.4             | 2               | 2.15            |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sample (adjusted)     | 1995Q2 2016Q4   | 2000Q2 2016Q4    | 1995Q2 2016Q4   | 1995Q2 2016Q4   | 1996Q2 2016Q4   |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Method                | Long run:       | Fully Modified   | Least Squares   | (FMOLS)         |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                       | Short run:      | Least Squares    |                 |                 |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+

Table: Estimates for extra export price equation specification, 2-step
estimation

Exports of goods and services, intra euro area, prices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculated via identity.

.. math::

   \label{eq:MND}
     \ln(XTD) = \alpha_{0} + \alpha_{xtd} \ln(XXD) + (1-\alpha_{xtd}) \ln(XND)

 where the share of extra euro area export prices is the following. (The
values are calculated based on what is used in the projections.)

+------------------------+-----------+----------+---------+---------+---------------+
|                        | Germany   | France   | Italy   | Spain   | Netherlands   |
+========================+===========+==========+=========+=========+===============+
| :math:`\alpha_{xtd}`   | 0.65      | 0.56     | 0.59    | 0.47    | 0.48          |
+------------------------+-----------+----------+---------+---------+---------------+

Imports of goods and services, prices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We decompose the prices of imported goods and services (:math:`MTD`)
into two parts, namely import deflator excluding energy
(:math:`MTD^{NO}`) and energy prices (:math:`P^{EI}`).

.. math::

   \label{eq:MTD}
     \ln(MTD) = \alpha_{0} + \alpha_{1} \ln(MTD^{NO}) + (1-\alpha_{1}) \ln(P^{EI})

 where the share of non-energy component is the following. (The values
are estimated on past data.)

+--------------------------+-----------+----------+---------+---------+---------------+
|                          | Germany   | France   | Italy   | Spain   | Netherlands   |
+==========================+===========+==========+=========+=========+===============+
| :math:`\alpha_{mtdno}`   | 0.93      | 0.89     | 0.92    | 0.76    | 0.89          |
+--------------------------+-----------+----------+---------+---------+---------------+

Import deflator excluding energy (:math:`MTD^{NO}`) is determined based
on the evolvement of competitors’ import prices (:math:`CMD`) and the
domestic price level at market prices (:math:`YED`). In the long run
([eq:MTDNO\_long]), we restrict the coefficients on the domestic prices
and the competitors prices to add up to one, assuming homogeneity in
prices. In most cases, the inclusion of a linear trend turned out to be
necessary in order to capture the oil price developments, and achieve
the stationary of the residual from the co-integrating equation. (Law of
one price seem to hold for :math:`MTD`, however, for forecasting
purposes, we need to model :math:`MTD^{NO}`. Trends are needed to be
included due to trending oil prices.)

In the short run ([eq:MTDNO\_short]), while prices accommodate to their
trends, we let them react differently to changes in costs, exchange rate
as well as competitors’ prices extra-(\ :math:`CMD^{EX}`) and intra-EA
(:math:`CMD^{IN}`).

.. math::

   \begin{aligned}
     \label{eq:MTDNO_long}
     \ln(MTD^{NO,LR}) &= \gamma_{0}+\gamma_{1} \ln(CMD)+ (1-\gamma_{1}) \ln\left(YED\right)    \nonumber \\
          &+ \gamma_{2}\cdot Trend                                                                       \\
     \label{eq:MTDNO_short}
     \Delta\ln(MTD^{NO}) &= \beta_{0} + \beta_{1} \Delta\ln(MTD^{NO}_{-1})                    \nonumber \\
          &+ \beta_{2} \Delta\ln(CMD^{IN}) + \beta_{3} \Delta\ln(CMD^{EX})                    \nonumber \\
          &+ \beta_{4} \left(\ln(MTD^{NO}_{-1}) - \ln(MTD^{NO,LR}_{-1})\right) + \epsilon^{PM,NO}\end{aligned}

| The estimated equations are the following:

+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                       | Germany         | France           | Italy           | Spain           | Netherlands     |
+=======================+=================+==================+=================+=================+=================+
| C                     | -2.23\*\*\*     | -2.47\*\*\*      | -1.08\*\*\*     | -2.84\*\*\*     | -2.77\*\*\*     |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Lin.trend             | -0.0027\*\*\*   | -0.0037\*\*\*    | 0.002\*\*\*     | -0.007\*\*\*    | -0.002\*\*\*    |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Domestic prices       | (1-0.56)        | (1-0.54)         | (1-0.80)        | (1-0.36)        | (1-0.57)        |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Competitors’ prices   | 0.56\*\*\*      | 0.54\*\*\*       | 0.8\*\*\*       | 0.36\*\*\*      | 0.57\*\*\*      |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| D09                   |                 |                  | 0.16\*\*\*      | -0.62\*\*\*     |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| D09\*Trend            |                 |                  | -0.003\*\*\*    | 0.011\*\*\*     |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| C                     | -0.001\*\*      | -0.002\*\*\*     | 0.002\*\*       | 0.001           | -0.001          |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| ECM                   | -0.31\*\*\*     | -0.07\*          | -0.37\*\*\*     | -0.69\*\*\*     | -0.16\*\*\*     |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| AR(1)                 | 0.26\*\*\*      | 0.23\*\*         | 0.19\*\*\*      |                 | -0.34           |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(CMD)                |                 |                  |                 |                 | 0.24\*\*\*      |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(CMDIN)              | 0.45\*\*\*      | 0.63\*\*\*       | 0.43\*\*\*      |                 |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(CMDEX)              | 0.07\*\*        |                  | 0.22\*\*\*      |                 |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Adj. R-squared        | 0.682           | 0.443            | 0.715           | 0.427           | 0.218           |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| S.E. of regression    | 0.0056          | 0.0064           | 0.0076          | 0.0143          | 0.0142          |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sum squared resid     | 0.0026          | 0.0033           | 0.0047          | 0.0174          | 0.0165          |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Durbin-Watson stat    | 2.23            | 2.25             | 1.62            | 1.74            | 1.8             |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sample (adjusted)     | 1995Q3-2016Q4   | 1995Q3-2016Q4    | 1995Q3-2016Q4   | 1995Q2-2016Q4   | 1995Q1-2016Q4   |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Method                | Long run:       | Fully Modified   | Least Squares   | (FMOLS)         |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                       | Short run:      | Least Squares    |                 |                 |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+

Table: Estimates for import price equation specification, 2-step
estimation

Imports of goods and services, extra euro area, prices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Extra euro area import prices are modelled as relative to the totals.

.. math::

   \begin{aligned}
     \label{eq:MXD_long}
     \ln(MXD^{LR}) &= \gamma_{0} + \ln(MTD) + \ln(CMD^{EX}/CMD)+ \gamma_{1} \ln(YED/CMD^{EX})                 \\
     \label{eq:MXD_short}
     \Delta\ln(MXD) &= \beta_{0} + \beta_{1} \Delta\ln(MXD_{-1}) + \beta_{2} \Delta\ln(MTD)                   \nonumber \\
          &+ \beta_{3} \Delta\ln(CMD^{EX}) + \beta_{4} \Delta\ln(CMD) + \beta_{5} \Delta\ln(P^{OIL,EUR})      \nonumber \\
          &+ \beta_{4} \left(\ln(MXD_{-1}) - \ln(MXD^{LR}_{-1})\right) + \epsilon^{PMX}\end{aligned}

+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                       | Germany         | France           | Italy           | Spain           | Netherlands     |
+=======================+=================+==================+=================+=================+=================+
| C                     | 2.58\*\*\*      | 4.23\*\*\*       | 3.9\*\*\*       | 3.37\*\*\*      | 4.59            |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Lin.trend             |                 | -0.0013\*\*\*    |                 |                 |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Domestic prices       | (1-0.54)        | (1-0.90)         | (1-0.84)        | (1-0.72)        |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Competitors’ prices   | 0.54\*\*\*      | 0.9\*\*\*        | 0.84\*\*\*      | 0.72\*\*\*      | -1              |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| C                     | -0.002\*\*      | -0.001           | 0.001           | 0.003           | -0.002\*        |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| ECM                   | -0.08\*\*\*     | -0.22\*\*\*      | -0.12\*         | -0.15\*\*       | -0.09\*\*       |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| AR(1)                 | 0.3\*\*\*       |                  |                 |                 |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(CMDIN)              | 0.88\*\*\*      | 0.76\*\*\*       | 0.61\*\*\*      |                 | 1.68\*\*\*      |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Adj. R-squared        | 0.714           | 0.187            | 0.501           | 0.064           | 0.678           |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| S.E. of regression    | 0.0057          | 0.0126           | 0.0081          | 0.016           | 0.008           |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sum squared resid     | 0.0027          | 0.0133           | 0.0056          | 0.0217          | 0.0051          |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Durbin-Watson stat    | 2.24            | 1.88             | 1.86            | 2.17            | 2.13            |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sample (adjusted)     | 1995Q3-2016Q4   | 1995Q2-2016Q4    | 1995Q2-2016Q4   | 1995Q2-2016Q4   | 1996Q1-2016Q4   |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Method                | Long run:       | Fully Modified   | Least Squares   | (FMOLS)         |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                       | Short run:      | Least Squares    |                 |                 |                 |
+-----------------------+-----------------+------------------+-----------------+-----------------+-----------------+

Table: Estimates for extra import price equation specification, 2-step
estimation

Imports of goods and services, intra euro area, prices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculated via identity.

.. math::

   \label{eq:MND}
     \ln(MTD) = \alpha_{0} + \alpha_{mtd} \ln(MXD) + (1-\alpha_{mtd}) \ln(MND)

 where the share of extra euro area import prices are the following.
(The values are calculated based on what is used in the projections.)

+------------------------+-----------+----------+---------+---------+---------------+
|                        | Germany   | France   | Italy   | Spain   | Netherlands   |
+========================+===========+==========+=========+=========+===============+
| :math:`\alpha_{mtd}`   | 0.62      | 0.59     | 0.52    | 0.50    | 0.63          |
+------------------------+-----------+----------+---------+---------+---------------+

Additional identities
---------------------

Price of Oil
~~~~~~~~~~~~

Price of oil in USD are used. Hence, in simulations change in EURUSD
exchange rate has a direct impact on energy prices via oil prices.

Price of Energy
~~~~~~~~~~~~~~~

Variable of Price of energy describes the price of fuel and energy,
which is part of import prices and consumer prices. For all of the
countries, it reacts by half to changes in oil prices in the short run,
which is in line with Lutz Kilian, who found the same size of reaction
for the US economy.

+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                      | Germany         | France           | Italy           | Spain           | Netherlands     |
+======================+=================+==================+=================+=================+=================+
| C                    | 0.62 \*\*\*     | -0.67 \*\*\*     | -10.61\*\*\*    | -7.8 \*\*\*     | 0.14 \*\*\*     |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Oil price            | 0.67 \*\*\*     | 0.67 \*\*\*      | 0.66 \*\*\*     | 0.76\*\*\*      | 0.64 \*\*\*     |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Commodity price      | (1-0.67)        | (1-0.67)         | (1-0.66)        | (1-0.76)        | (1-0.64)        |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| C                    | 0.005 \*\*\*    | 0.005\*\*        | 0.005           | 0.008\*\*\*     | 0.002           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| ECM                  | -0.02           | -0.01            | -0.04           | -0.01           | -0.05\*         |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(POil)              | 0.49\*\*\*      | 0.5\*\*\*        | 0.54\*\*\*      | 0.48\*\*\*      | 0.49\*\*\*      |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| d(PComm)             | 0.39\*\*\*      | 0.34\*\*\*       | 0.26\*\*\*      | 0.32\*\*\*      | 0.38 \*\*\*     |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Adj. R-squared       | 0.974           | 0.962            | 0.908           | 0.938           | 0.942           |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| S.E. of regression   | 0.0142          | 0.0174           | 0.0282          | 0.0215          | 0.0216          |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sum squared resid    | 0.016           | 0.0238           | 0.0627          | 0.0364          | 0.0369          |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Durbin-Watson stat   | 1.98            | 1.67             | 2.1             | 2.37            | 1.59            |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Sample (adjusted)    | 1996Q2 2016Q4   | 1996Q2 2016Q4    | 1996Q2 2016Q4   | 1996Q2 2016Q4   | 1996Q2 2016Q4   |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
| Method               | Long run:       | Fully Modified   | Least Squares   | (FMOLS)         |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+
|                      | Short run:      | Least Squares    |                 |                 |                 |
+----------------------+-----------------+------------------+-----------------+-----------------+-----------------+

Table: Estimates for extra import price equation specification, 2-step
estimation

Current account and net foreign assets
--------------------------------------

Current account
~~~~~~~~~~~~~~~

Net foreign assets
~~~~~~~~~~~~~~~~~~

.. |Structure of the Foreign Block| image:: flow_FB.png
