.. role:: math(raw)
   :format: html latex
..

.. contents::
   :depth: 3
..

Macro-Financial Linkages [MFL]
==============================

In this section we describe the two main channels through with financial
variables can affect the rest of the economy: the household wealth
channel and the cost of external funds for NFC. The first variable
affect the dynamics of consumption. The latter of capital investment.

Household Wealth (:math:`HHW_{t}`) [HHW]
----------------------------------------

Household sector wealth is defined as follows

.. math:: HHW_{t}=GHW_{t}+NFW_{t}  \label{HHW}

 where :math:`GHW_{t}` is the gross housing wealth :math:`NFW_{t}` is
net financial wealth

The stock of **nominal housing wealth** is revalued in line with changes
in house prices (:math:`HP_{t}`)

.. math::

   GHW_{t} = IH_{t} + (1-Depr)IH_{t-1}(HP_{t}/HP_{t-1})
    \label{GHW}

 where :math:`IH_{t}` is Nominal Residential Investment (gross capital
formation, household sector)  [1]_ ; :math:`HP_{t}` is residential
property prices (New and existing dwellings; Residential property in
good and poor condition) and :math:`\textit{Depr}` is the depreciation
rate for housing stock and it is assumed to be equal to YYY. Land and
other non-modelled components are ignored.The discrepancy between the
model implied and actual Housing wealth (:math:`QGHW_{t}`\ =
:math:`GHW_{t,model}`/ :math:`GHW_{t,data}`) is calculated and used in
simulations as correction factor.

Household net financial wealth (:math:`NFW_{t}`) is modelled as follows

.. math::

   NFW_{t} = (Y_{t} -C_{t}- IH_{t}) +NFW_{t-1}REV_{t}
    \label{NFW}

where :math:`Y_{t}` is the Nominal Disposable Income, :math:`C_{t}` is
the Nominal (total) Consumption, :math:`IH_{t}` is the Nominal
Residential Investment (gross fixed capital formation, dwellings) and
:math:`REV_{t}` is the Revaluation Term.

Net financial wealth includes several types of assets (treasury bonds,
equities, deposits, foreign assets, other debt securities …) that need
to be re-valuated with the changes in different asset prices. The
revaluation term will be a weighted average of the change in the
corresponding asset prices (equity prices and debt securities) with the
weights reflecting the relative importance of the various components in
the asset holding of households:

.. math:: REV_{t}=c+a^{GB} (R_{t-1}^{GB}/R_{t}^{GB}) +a^{CB} (R_{t-1}^{CB}/R_{t}^{CB}) + a^{EQ} (EQP_{t}/EQP_{t-1})

 where :math:`c` is the fraction of assets not subject to revaluation
(mostly deposits with banks), :math:`a^{GB}` is the fraction of
government debt securities (held directly and indirectly by household)
in total financial assets, :math:`a^{CB}` is the fraction of corporate
debt securities (held directly and indirectly by household) in total
financial assets and :math:`a^{EQ}` is the fraction of equities (listed
Equities + half of the unlisted equities held by households) in total
financial assets, whereas :math:`R_{t}^{GB}`\ is the domestic long-term
bond yield (10YR), :math:`R_{t}^{CB}` is the domestic corporate bond
yield and :math:`EQP_{t}` is the domestic stock market price index.

| l \*4Y & **c** & **:math:`a^{LT}`** & **:math:`a^{CB}`** &
  **:math:`a^{EQP}`**
| (r)1-1(r)2-2(r)3-3(r)4-4(r)5-5 DE & 67.8 & 2 & 16.3 & 13.9
| ES & 51.7 & 6.5 & 22.1 & 19.7
| FR & 53.3 & 14.8 & 16.8 & 15.2
| IT & 66.8 & 6.1 & 7.8 & 19.3
| NL & 57 & 3.9 & 18.1 & 21
| & & & &

NFC Cost of External Funds [NFC\_COF]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Loans, debt security and equity represent the external funds of NFC. The
overall NFC external financing cost is modelled as a weighted average of
the three different funding costs based on the respective amounts
outstanding derived from sectoral accounts (QSA). [2]_

| l \*5Y & **Loans\*** & **Debt Securities** & **Equity\***
| (r)1-1(r)2-2(r)3-3(r)4-4 & & &
| EA & 56 & 5 & 40
| DE & 52 & 5 & 43
| FR & 41 & 14 & 45
| IT & 69 & 5 & 26
| ES & 68 & 2 & 30
| NL & 67 & 9 & 24
| & & &

.. [1]
   See Appex...For an alternative formulation that uses gross fixed
   capital formation, dwellings, total economy

.. [2]
   For the definitions, see Appendix XX. For the cost of equity see
   Appendix XX.
