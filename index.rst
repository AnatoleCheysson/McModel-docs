.. Multi country model documentation master file, created by
   sphinx-quickstart on Sat Apr 29 22:01:31 2010.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. header:: For comments please contact `Nikolay Iskrev <mailto:nikolay.iskrev@ecb.europa.eu?subject=MCmodel%20Documentation%20comment>`_

########################
ECB Multicountry Model
########################

:Last Updated: |today|

The purpose of this document is to provide an overview of the ECB Multicountry (ECB-MC) model. The ECB-MC model is a large-scale model of the Euro area economy featuring optimizing behavior by households and firms as well as detailed descriptions of monetary policy and the fiscal sector. The model's large number of endogenous variables permits the study of the effects of a broad range of macroeconomic policies and exogenous shocks on real GDP and its major spending components; the unemployment rate and other key labor market indicators; several measures of inflation and relative prices; the main categories of national income; a detailed treatment of the government's account; and various interest rates, asset prices, and components of wealth. ECB-MC has a neoclassical core that combines a production function with endogenous and exogenous supplies of production factors and key aspects of household preferences such as impatience. To account for cyclical fluctuations, the model features rigidities that apply to many decisions made by households and firms; these rigidities enable the model to generate gradual responses of macroeconomic variables to a wide range of exogenous shocks that are consistent with the economic data.

A detailed description of the model's equations is provided in several working papers. Here we provide only an overview of the main specifications of the various agents' behavior.


Contents:
=========

.. toctree::
   :maxdepth: 2
   :numbered:
   :glob:

   rstFiles/*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
