.. _ea_risk_free_treasury_term_structure:

************************************
EA Risk Free Treasury Term Structure
************************************


.. image:: /_static/images/EA_yield_curve_3D.png
   :height: 1000 px
   :width: 1000 px
   :scale: 70 %
   :alt: alternate text
   :align: center
